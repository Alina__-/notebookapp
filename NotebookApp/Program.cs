﻿using System;
using System.Collections.Generic;

namespace NotebookApp
{
    class Notebook
    {
        public static List<Note> notebook = new List<Note>();
        public class Note
        {
            private string surname;
            private string name;
            private string middleName;
            private string phoneNumber;
            private string country;
            private string date;
            private string organisation;
            private string position;
            private string other;
            public string Surname
            {
                get { return surname; }
                set { this.surname = value; }
            }
            public string Name
            {
                get { return name; }
                set { this.name = value; }
            }
            public string MiddleName
            {
                get { return middleName; }
                set { this.middleName = value; }
            }
            public string PhoneNumber
            {
                get { return phoneNumber; }
                set { this.phoneNumber = value; }
            }
            public string Сountry
            {
                get { return country; }
                set { this.country = value; }
            }
            public string Date
            {
                get { return date; }
                set { this.date = value; }
            }
            public string Organisation
            {
                get { return organisation; }
                set { this.organisation = value; }
            }
            public string Position
            {
                get { return position; }
                set { this.position = value; }
            }
            public string Other
            {
                get { return other; }
                set { this.other = value; }
            }
        }
        public static Note CreateNewNote()
        {
            Note note = new Note();
            Console.Clear();
            Console.WriteLine("Введите фамилию");
            note.Surname = Console.ReadLine();
            Console.Clear();
            Console.WriteLine("Введите имя");
            note.Name = Console.ReadLine();
            note.MiddleName = "-";
            Console.Clear();
            Console.WriteLine("Введите номер телефона (без пробелов). Используйте только цифры.");
            string PhoneNumberString = Console.ReadLine();
            bool boba = true;
            while (boba == true)
            {
                boba = false;
                char[] a = PhoneNumberString.ToCharArray();
                for (int i = 0; i < a.Length; ++i)
                {
                    if (!Char.IsNumber(a[i]))
                    {
                        Console.WriteLine("Попробуйте ещё раз.");
                        PhoneNumberString = Console.ReadLine();
                        boba = true;
                        break;
                    }
                }
            }
            note.PhoneNumber = PhoneNumberString;
            note.Сountry = "-";
            note.Date = "-";
            note.Organisation = "-";
            note.Position = "-";
            note.Other = "-";
            notebook.Add(note);
            Console.Clear();
            return note;

        }
        public static void EditNote(Note note)
        {
            bool bobaEditNote = true;
            while (bobaEditNote == true)
            {
                Console.Clear();
                bobaEditNote = false;
                Console.WriteLine("Введите имя одного пункта (с маленькой буквы), который Вы бы хотели изменить.");
                Console.WriteLine("фамилия/имя/отчество/номер телефона/страна/дата рождения/организация/должность/прочие заметки");
                string editNoteAnswer = Console.ReadLine();
                switch (editNoteAnswer)
                {
                    case "фамилия":
                        Console.Clear();
                        Console.WriteLine("Введите фамилию");
                        note.Surname = Console.ReadLine();
                        break;
                    case "имя":
                        Console.Clear();
                        Console.WriteLine("Введите имя");
                        note.Name = Console.ReadLine();
                        break;
                    case "отчество":
                        Console.Clear();
                        Console.WriteLine("Введите отчество");
                        note.MiddleName = Console.ReadLine();
                        break;
                    case "номер телефона":
                        Console.Clear();
                        Console.WriteLine("Введите номер телефона (без пробелов). Используйте только цифры.");
                        string PhoneNumberString = Console.ReadLine();
                        bool boba = true;
                        while (boba == true)
                        {
                            boba = false;
                            char[] a = PhoneNumberString.ToCharArray();
                            for (int i = 0; i < a.Length; ++i)
                            {
                                if (!Char.IsNumber(a[i]))
                                {
                                    Console.WriteLine("Попробуйте ещё раз.");
                                    PhoneNumberString = Console.ReadLine();
                                    boba = true;
                                    break;
                                }
                            }
                        }
                        break;
                    case "страна":
                        Console.Clear();
                        Console.WriteLine("Введите страну");
                        note.Сountry = Console.ReadLine();
                        break;
                    case "дата рождения":
                        Console.Clear();
                        Console.WriteLine("Вы выбрали дату рождения." + "\n" + "Введите день рождения:");
                        string dayBirth = Console.ReadLine();
                        string date = "";
                        int dayBirthInt = 0;
                        bool bobaboba = true;
                        while (bobaboba)
                        {
                            bobaboba = false;
                            while (!Int32.TryParse(dayBirth, out dayBirthInt))
                            {
                                Console.WriteLine("Введите корректное число.");
                                dayBirth = Console.ReadLine();
                                bobaboba = true;
                            }
                            while (Int32.TryParse(dayBirth, out dayBirthInt))
                            {
                                if ((dayBirthInt > 0) && dayBirthInt <= 31)
                                {
                                    break;
                                }
                                else
                                {
                                    bobaboba = true;
                                    Console.WriteLine("Введите корректное число.");
                                    dayBirth = Console.ReadLine();
                                }
                            }
                        }
                        Console.Clear();
                        Console.WriteLine("Введите месяц рождения:");
                        string monthBirth = Console.ReadLine();
                        int monthBirthInt = 0;
                        bobaboba = true;
                        while (bobaboba)
                        {
                            bobaboba = false;
                            while (!Int32.TryParse(monthBirth, out monthBirthInt))
                            {
                                Console.WriteLine("Введите корректное число.");
                                monthBirth = Console.ReadLine();
                                bobaboba = true;
                            }
                            while (Int32.TryParse(monthBirth, out monthBirthInt))
                            {
                                if ((monthBirthInt > 0) && monthBirthInt <= 12)
                                {
                                    break;
                                }
                                else
                                {
                                    bobaboba = true;
                                    Console.WriteLine("Введите корректное число.");
                                    monthBirth = Console.ReadLine();
                                }
                            }
                        }
                        Console.Clear();
                        Console.WriteLine("Введите год рождения (в интервале от 1900 до 2020 включительно):");
                        string yearBirth = Console.ReadLine();
                        int yearBirthInt = 0;
                        bobaboba = true;
                        while (bobaboba)
                        {
                            bobaboba = false;
                            while (!Int32.TryParse(yearBirth, out yearBirthInt))
                            {
                                Console.WriteLine("Введите корректное число.");
                                yearBirth = Console.ReadLine();
                                bobaboba = true;
                            }
                            while (Int32.TryParse(yearBirth, out yearBirthInt))
                            {
                                if ((yearBirthInt >= 1900) && yearBirthInt <= 2020)
                                {
                                    break;
                                }
                                else
                                {
                                    bobaboba = true;
                                    Console.WriteLine("Введите корректное число.");
                                    yearBirth = Console.ReadLine();
                                }
                            }
                        }
                        date = dayBirth + "." + monthBirth + "." + yearBirth;
                        note.Date = date;
                        break;
                    case "организация":
                        Console.Clear();
                        Console.WriteLine("Введите наименование организации:");
                        note.Organisation = Console.ReadLine();
                        break;
                    case "должность":
                        Console.Clear();
                        Console.WriteLine("Введите должность:");
                        note.Position = Console.ReadLine();
                        break;
                    case "прочие заметки":
                        Console.Clear();
                        Console.WriteLine("Введите любую дополнительную информацию:");
                        note.Other = Console.ReadLine();
                        break;
                    default:
                        Console.WriteLine("Попробуйте ещё раз.");
                        editNoteAnswer = Console.ReadLine();
                        continue;
                }
                Console.Clear();
                Console.WriteLine("Продолжить редактирование?" + "\n" + "Введите: да/нет");
                string editNoteAnswer2 = Console.ReadLine();
                switch (editNoteAnswer2)
                {
                    case "да":
                        bobaEditNote = true;
                        break;
                    case "нет":
                        bobaEditNote = false;
                        break;
                    default:
                        Console.WriteLine("Попробуйте еще раз.");
                        continue;
                }
            }
        }
        public static void DeleteNote(Note note)
        {
            notebook.Remove(note);
        }
        public static void ReadNote(Note note)
        {
            Console.Clear();
            Console.WriteLine("Фамилия: " + note.Surname);
            Console.WriteLine("Имя: " + note.Name);
            Console.WriteLine("Отчество: " + note.MiddleName);
            Console.WriteLine("Номер телефона: " + note.PhoneNumber);
            Console.WriteLine("Страна: " + note.Сountry);
            Console.WriteLine("Дата рождения: " + note.Date);
            Console.WriteLine("Организация: " + note.Organisation);
            Console.WriteLine("Должность: " + note.Position);
            Console.WriteLine("Прочие заметки: " + note.Other);
        }
        public static void ShowAllNotes()
        {
            foreach (Note note in notebook)
            {
                Console.WriteLine("Фамилия:" + note.Surname + "\n" + "Имя:" + note.Name + "\n" + "Номер телефона:" + note.PhoneNumber);
                Console.WriteLine();
            }
        }
        static void Main(string[] args)
        {
            if (true)
            {
                Console.WriteLine("Привет! Я записная телефонная книжка!" + "\n" + "Хотите создать новую запись?");
                bool bobaboba = true;
                int cout = 0;
                Note note1 = new Note();
                Console.WriteLine("Введите: да/нет");
                string answer = Console.ReadLine();
                while (bobaboba == true)
                {
                    bobaboba = false;
                    switch (answer)
                    {
                        case "да":
                            Console.Clear();
                            note1 = Notebook.CreateNewNote();
                            Console.WriteLine("Ура! Вы создали новую запись.");
                            cout++;
                            break;
                        case "нет":
                            Console.Clear();
                            Console.WriteLine("Хорошо, перейдем к следующему пункту...");
                            break;
                        default:
                            Console.WriteLine("Вы ввели неверные данные. Перейдем к следующему пункту...");
                            break;
                    }
                    if (cout == 0)
                    {
                        Console.WriteLine("Ой, записи отсутствуют... Приходите снова!");
                        break;
                    }
                    else if (cout > 1)
                    {
                        Console.WriteLine("Хотите ли перейти к работе с другой записью?");
                        Console.WriteLine("Введите: да/нет");
                        answer = Console.ReadLine();
                        switch (answer)
                        {
                            case "да":
                                Console.Clear();
                                Console.WriteLine("Количество созданных записей: " + notebook.Count + ". Введите номер записи, с которой Вы бы хотели продолжить работу.");
                                int numberInt = 0;
                                string number = Console.ReadLine();
                                bobaboba = true;
                                while (bobaboba)
                                {
                                    bobaboba = false;
                                    while (!Int32.TryParse(number, out numberInt))
                                    {
                                        Console.WriteLine("Введите корректное число.");
                                        number = Console.ReadLine();
                                        bobaboba = true;
                                        continue;
                                    }
                                    while (Int32.TryParse(number, out numberInt))
                                    {
                                        if ((numberInt <= notebook.Count) && numberInt > 0)
                                        {
                                            break;
                                        }
                                        else
                                        {
                                            bobaboba = true;
                                            Console.WriteLine("Введите корректное число.");
                                            number = Console.ReadLine();
                                            continue;
                                        }
                                    }
                                    for (int i = 0; i < notebook.Count; i++)
                                    {
                                        if (numberInt - 1 == i)
                                        {
                                            note1 = notebook[i];
                                            Console.Clear();
                                            Console.WriteLine("Вы выбрали " + number + " запись!");
                                        }
                                    }
                                }
                                break;
                            case "нет":
                                Console.Clear();
                                Console.WriteLine("Хорошо, перейдем к следующему пункту...");
                                break;
                            default:
                                Console.WriteLine("Вы ввели неверные данные. Значит этот пункт опустим...");
                                break;
                        }
                    }
                    Console.WriteLine("Хотите ли Вы редактировать текущую запись?");
                    Console.WriteLine("Введите: да/нет");
                    answer = Console.ReadLine();
                    switch (answer)
                    {
                        case "да":
                            Console.Clear();
                            Notebook.EditNote(note1);
                            Console.Clear();
                            Console.WriteLine("Готово! Вы закончили редактировать запись.");
                            break;
                        case "нет":
                            Console.Clear();
                            Console.WriteLine("Хорошо, перейдем к следующему пункту...");
                            break;
                        default:
                            Console.WriteLine("Вы ввели неверные данные. Значит этот пункт опустим...");
                            break;
                    }
                    Console.WriteLine("Хотите ли Вы посмотреть все данные текущей записи?");
                    Console.WriteLine("Введите: да/нет");
                    answer = Console.ReadLine();
                    switch (answer)
                    {
                        case "да":
                            Notebook.ReadNote(note1);
                            Console.WriteLine("\n" + "Готово! Так выглядит запись о текущем человеке.");
                            break;
                        case "нет":
                            Console.Clear();
                            Console.WriteLine("Хорошо, перейдем к следующему пункту...");
                            break;
                        default:
                            Console.WriteLine("Вы ввели неверные данные. Значит этот пункт опустим...");
                            break;
                    }
                    Console.WriteLine("Нажмите любую клавишу для продолжения...");
                    Console.ReadKey();
                    Console.Clear();
                    Console.WriteLine("Хотите ли Вы удалить текущую запись?");
                    Console.WriteLine("Введите: да/нет");
                    answer = Console.ReadLine();
                    switch (answer)
                    {
                        case "да":
                            Console.Clear();
                            Notebook.DeleteNote(note1);
                            Console.WriteLine("Готово! Текущая запись удалена.");
                            cout = cout-1;
                            break;
                        case "нет":
                            Console.Clear();
                            Console.WriteLine("Хорошо, перейдем к следующему пункту...");
                            break;
                        default:
                            Console.WriteLine("Вы ввели неверные данные. Значит этот пункт опустим...");
                            break;
                    }
                    Console.WriteLine("Хотите ли Вы посмотреть все созданные записи?");
                    Console.WriteLine("Введите: да/нет");
                    answer = Console.ReadLine();
                    switch (answer)
                    {
                        case "да":
                            Console.Clear();
                            Notebook.ShowAllNotes();
                            Console.WriteLine("\n" + "Готово! Так выглядит вся важная информация о созданных записях.");
                            break;
                        case "нет":
                            Console.Clear();
                            Console.WriteLine("Хорошо, перейдем к следующему пункту...");
                            break;
                        default:
                            Console.Clear();
                            Console.WriteLine("Вы ввели неверные данные. Значит этот пункт опустим...");
                            break;
                    }
                    Console.WriteLine("Хотите ли Вы закрыть приложение?");
                    Console.WriteLine("Введите: да/нет");
                    answer = Console.ReadLine();
                    if (answer == "да")
                    {
                        Console.Clear();
                        Console.WriteLine("На этом моя работа закончена. До свидания!");
                        break;
                    }
                    else if (answer == "нет")
                    {
                        Console.Clear();
                        Console.WriteLine("Ура! Давайте продолжим!");
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("Я Вас не понимаю... До свидания!");
                        break;
                    }
                    Console.WriteLine("Хотите ли Вы создать новую запись?");
                    Console.WriteLine("Введите: да/нет");
                    answer = Console.ReadLine();
                    switch (answer)
                    {
                        case "да":
                            bobaboba = true;
                            break;
                        case "нет":
                            bobaboba = true;
                            break;
                        default:
                            Console.Clear();
                            Console.WriteLine("Вы ввели неверные данные. Значит этот пункт опустим...");
                            bobaboba = true;
                            break;
                    }
                }
            }
        }
    }
}

